import { readFile } from 'node:fs/promises'

interface RangeIntersect {
  newRanges: RangeInterface[];
  type: RangeIntersectEnum;
}

interface RangeInterface {
  readonly end: number;
  start: number;
  range: number;

  clone(): RangeInterface;

  convert(diffStart: number): RangeInterface;

  intersect(range: RangeInterface): RangeIntersect;
}

interface ConvertMap {
  rangeSource: RangeInterface;
  rangeDestination: RangeInterface;
}

type ConvertionType =
    | 'seedToSoil'
    | 'soilToFertilizer'
    | 'fertilizerToWater'
    | 'waterToLight'
    | 'lightToTemperature'
    | 'temperatureToHumidity'
    | 'humidityToLocation'
    | 'end'

enum RangeIntersectEnum {
  A_BEFORE_B,
  A_INTERSECT_B_START,
  A_INCLUDE_B,
  A_EQUAL_B,
  B_INCLUDE_A,
  A_INTERSECT_B_END,
  A_AFTER_B,
}

class Range implements RangeInterface {
  #start: number
  readonly #end: number
  readonly #range: number

  constructor(start: number, range: number) {
    this.#start = start
    this.#range = range
    this.#end = start + range - 1
  }

  get start(): number {
    return this.#start
  }

  get end(): number {
    return this.#end
  }

  get range(): number {
    return this.#range
  }

  intersect(B: RangeInterface): RangeIntersect {
    if (this.#end < B.start) return { newRanges: [this], type: RangeIntersectEnum.A_BEFORE_B }
    if (this.#start > B.end) return { newRanges: [this], type: RangeIntersectEnum.A_AFTER_B }

    if (this.#start === B.start && this.#end === B.end) {
      return {
        newRanges: [this],
        type: RangeIntersectEnum.A_EQUAL_B,
      }
    }

    if ((this.#start <= B.start && this.#end > B.end) || (this.#start < B.start && this.#end >= B.end)) {
      return {
        newRanges: [
          new Range(this.#start, B.start - this.#start),
          new Range(B.start, B.end - B.start + 1),
          new Range(B.end + 1, this.#end - B.end),
        ],
        type: RangeIntersectEnum.A_INCLUDE_B,
      }
    }

    if ((this.#start >= B.start && this.#end < B.end) || (this.#start > B.start && this.#end <= B.end)) {
      return {
        newRanges: [this],
        type: RangeIntersectEnum.B_INCLUDE_A,
      }
    }

    if (this.#start < B.start && this.#end < B.end) {
      return {
        newRanges: [
          new Range(this.#start, B.start - this.#start),
          new Range(B.start, this.#end - B.start + 1),
        ],
        type: RangeIntersectEnum.A_INTERSECT_B_START,
      }
    }
    if (this.#start > B.start && this.#end > B.end) {
      return {
        newRanges: [
          new Range(this.#start, B.end - this.#start + 1),
          new Range(B.end + 1, this.#end - B.end),
        ],
        type: RangeIntersectEnum.A_INTERSECT_B_END,

      }
    }
  }

  convert(diffStart: number): RangeInterface {
    return new Range(this.#start + diffStart, this.#range)
  }

  clone(): RangeInterface {
    return new Range(this.#start, this.#range)
  }
}

class Garden {
  #ranges: RangeInterface[] = []
  #seedToSoilMap: ConvertMap[] = []
  #soilToFertilizerMap: ConvertMap[] = []
  #fertilizerToWaterMap: ConvertMap[] = []
  #waterToLightMap: ConvertMap[] = []
  #lightToTemperatureMap: ConvertMap[] = []
  #temperatureToHumidityMap: ConvertMap[] = []
  #humidityToLocationMap: ConvertMap[] = []
  #type: ConvertionType = 'seedToSoil'
  #i = 0

  constructor(data: string) {
    let isSeedToSoilMap = false
    let isSoilToFertilizerMap = false
    let isFertilizerToWaterMap = false
    let isWaterToLightMap = false
    let isLightToTemperatureMap = false
    let isTemperatureToHumidityMap = false
    let isHumidityToLocationMap = false

    const toggleCondition = (conditionName: string): void => {
      isSeedToSoilMap = conditionName === 'isSeedToSoilMap'
      isSoilToFertilizerMap = conditionName === 'isSoilToFertilizerMap'
      isFertilizerToWaterMap = conditionName === 'isFertilizerToWaterMap'
      isWaterToLightMap = conditionName === 'isWaterToLightMap'
      isLightToTemperatureMap = conditionName === 'isLightToTemperatureMap'
      isTemperatureToHumidityMap = conditionName === 'isTemperatureToHumidityMap'
      isHumidityToLocationMap = conditionName === 'isHumidityToLocationMap'
    }

    const initMap = (map: ConvertMap[], payload: [string, string, string]): void => {
      const [destination, source, range] = payload.map(Number)
      map.push({
        rangeDestination: new Range(destination, range),
        rangeSource: new Range(source, range),
      })
    }

    data.split('\n').forEach((line, i) => {
      if (line === '') return
      if (i === 0) {
        const items = line.split(': ')[1].split(' ').map(Number)

        for (let j = 0; j < items.length; j += 2) {
          this.#ranges.push(new Range(items[j], items[j + 1]))
        }
        return
      }

      switch (line) {
        case 'seed-to-soil map:':
          toggleCondition('isSeedToSoilMap')
          return
        case 'soil-to-fertilizer map:':
          toggleCondition('isSoilToFertilizerMap')
          return
        case 'fertilizer-to-water map:':
          toggleCondition('isFertilizerToWaterMap')
          return
        case 'water-to-light map:':
          toggleCondition('isWaterToLightMap')
          return
        case 'light-to-temperature map:':
          toggleCondition('isLightToTemperatureMap')
          return
        case 'temperature-to-humidity map:':
          toggleCondition('isTemperatureToHumidityMap')
          return
        case 'humidity-to-location map:':
          toggleCondition('isHumidityToLocationMap')
          return
      }

      if (isSeedToSoilMap) {
        initMap(this.#seedToSoilMap, line.split(' '))
        return
      }
      if (isSoilToFertilizerMap) {
        initMap(this.#soilToFertilizerMap, line.split(' '))
        return
      }
      if (isFertilizerToWaterMap) {
        initMap(this.#fertilizerToWaterMap, line.split(' '))
        return
      }
      if (isWaterToLightMap) {
        initMap(this.#waterToLightMap, line.split(' '))
        return
      }
      if (isLightToTemperatureMap) {
        initMap(this.#lightToTemperatureMap, line.split(' '))
        return
      }
      if (isTemperatureToHumidityMap) {
        initMap(this.#temperatureToHumidityMap, line.split(' '))
        return
      }
      if (isHumidityToLocationMap) {
        initMap(this.#humidityToLocationMap, line.split(' '))
        return
      }
    })

    this.#seedToSoilMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#soilToFertilizerMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#fertilizerToWaterMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#waterToLightMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#lightToTemperatureMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#temperatureToHumidityMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
    this.#humidityToLocationMap.sort((a, b) => a.rangeSource.start - b.rangeSource.start)
  }

  nearestSeed(): number {
    let i = 0

    while (this.#type !== 'end' && i < 50) {
      let newRanges: RangeInterface[] = []
      for (const range of this.#ranges) {
        newRanges = [...newRanges, ...this.#seedsToLocation(range)]
      }

      this.#ranges = [...newRanges]
      this.#changeType()
      i++
    }

    return this.#ranges.sort((a, b) => a.start - b.start)[0].start
  }

  #seedsToLocation(range: RangeInterface): RangeInterface[] {
    let newRanges: RangeInterface[] = []
    let intersect: RangeIntersect

    for (const convertMap of this.#getMap()) {
      intersect = range.intersect(convertMap.rangeSource)

      if (intersect.type === RangeIntersectEnum.A_AFTER_B) {
        continue
      }

      if (intersect.type === RangeIntersectEnum.A_BEFORE_B) {
        return [range.clone()]
      }

      if (intersect.type === RangeIntersectEnum.A_INTERSECT_B_START) {
        const [range1, range2] = intersect.newRanges
        newRanges.push(...this.#seedsToLocation(range1))

        newRanges.push(range2.convert(convertMap.rangeDestination.start - convertMap.rangeSource.start))
        return newRanges
      }

      if (intersect.type === RangeIntersectEnum.A_INTERSECT_B_END) {
        const [range1, range2] = intersect.newRanges
        newRanges.push(range1.convert(convertMap.rangeDestination.start - convertMap.rangeSource.start))

        newRanges.push(...this.#seedsToLocation(range2))
        return newRanges
      }

      if (intersect.type === RangeIntersectEnum.A_INCLUDE_B) {
        const [range1, range2, range3] = intersect.newRanges
        newRanges.push(...this.#seedsToLocation(range1))

        newRanges.push(range2.convert(convertMap.rangeDestination.start - convertMap.rangeSource.start))

        newRanges.push(...this.#seedsToLocation(range3))
        return newRanges
      }

      if (intersect.type === RangeIntersectEnum.B_INCLUDE_A || intersect.type === RangeIntersectEnum.A_EQUAL_B) {
        const [range] = intersect.newRanges
        newRanges.push(range.convert(convertMap.rangeDestination.start - convertMap.rangeSource.start))

        return newRanges
      }
    }

    return [range.clone()]
  }

  #changeType() {
    switch (this.#type) {
      case 'seedToSoil':
        this.#type = 'soilToFertilizer'
        return
      case 'soilToFertilizer':
        this.#type = 'fertilizerToWater'
        return
      case 'fertilizerToWater':
        this.#type = 'waterToLight'
        return
      case 'waterToLight':
        this.#type = 'lightToTemperature'
        return
      case 'lightToTemperature':
        this.#type = 'temperatureToHumidity'
        return
      case 'temperatureToHumidity':
        this.#type = 'humidityToLocation'
        return
      case 'humidityToLocation':
        this.#type = 'end'
        return
    }
  }

  #getMap(): ConvertMap[] {
    switch (this.#type) {
      case 'seedToSoil':
        return this.#seedToSoilMap
      case 'soilToFertilizer':
        return this.#soilToFertilizerMap
      case 'fertilizerToWater':
        return this.#fertilizerToWaterMap
      case 'waterToLight':
        return this.#waterToLightMap
      case 'lightToTemperature':
        return this.#lightToTemperatureMap
      case 'temperatureToHumidity':
        return this.#temperatureToHumidityMap
      case 'humidityToLocation':
        return this.#humidityToLocationMap
    }
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day5.txt', 'utf8')
  const garden = new Garden(data as string)
  return garden.nearestSeed()
}