import { readFileSync } from 'node:fs'
import path from 'node:path'

type Colors = 'red' | 'green' | 'blue'

interface GameSet {
  nb: number;
  color: Colors;
}

type GameSets = GameSet[][]

interface Games {
  gameIndex: number;
  gameSets: GameSets;
}

class MyGame {
  readonly red: number
  readonly green: number
  readonly blue: number
  readonly #games: Games[]

  constructor() {
    this.#games = readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' })
        .split('\n')
        .map(game => {
          const [gameIndex, gameSets] = game.split(': ')
          return {
            gameSets: gameSets.split('; ')
                              .map(gameSet => {
                                return gameSet.split(', ')
                                              .map(set => {
                                                const [nb, color] = set.split(' ')
                                                return {
                                                  nb: parseInt(nb, 10),
                                                  color: color as Colors,
                                                }
                                              })
                              }),
            gameIndex: parseInt(gameIndex.split(' ')[1], 10),
          }
        })
    this.red = 12
    this.green = 13
    this.blue = 14
  }

  checkGames = (): number => {
    return this.#games.reduce((sum, game) => {
      return sum + this.#gameCheck(game.gameSets)
    }, 0)
  }

  #gameCheck = (gameSets: GameSets): number => {
    const colors = {
      red: 0,
      green: 0,
      blue: 0,
    }

    for (const gameSet of gameSets) {
      for (const set of gameSet) {
        if (colors[set.color] < set.nb) colors[set.color] = set.nb
      }
    }

    return Object.values(colors).reduce((product, color) => product * color, 1)
  }
}

export const dayFunction = async () => {
  const game = new MyGame()
  return game.checkGames()
}