import { readFile } from 'node:fs/promises'

type Symbol = '.' | '#'

class MirrorMap {
  readonly #map: Symbol[][] = []
  #value = 0

  constructor(data: string) {
    const lines = data.split('\n')
    for (let y = 0; y < lines.length; y++) {
      this.#map.push(lines[y].split('') as Symbol[])
    }

    this.#findSymmetryLine()
  }

  get value(): number {
    return this.#value
  }

  #findSymmetryLine(): Promise<void> {
    return new Promise(resolve => {
      if (this.#testHorizontalSymmetry()) {
        resolve()
        return
      }

      this.#testVerticalSymmetry()
      resolve()
    })
  }

  #testHorizontalSymmetry(): boolean {
    for (let y = 0; y < this.#map.length - 1; y++) {
      if (this.#testHorizontalTowLines(y)) {
        this.#value = (y + 1) * 100
        return true
      }
    }

    return false
  }

  #testHorizontalTowLines(y: number): boolean {
    for (let x = 0; x < this.#map[0].length; x++) {
      if (this.#map[y][x] !== this.#map[y + 1][x]) {
        return false
      }
    }

    return this.#testHorizontalRestOfLines(y, 1, true)
  }

  #testHorizontalRestOfLines(y: number, i: number, lastValue: boolean): boolean {
    if ((y - i) < 0 || (y + 1 + i) >= this.#map.length) return lastValue

    for (let x = 0; x < this.#map[0].length; x++) {
      if (this.#map[y - i][x] !== this.#map[y + 1 + i][x]) {
        return false
      }
    }

    return this.#testHorizontalRestOfLines(y, i + 1, true)
  }

  #testVerticalSymmetry(): boolean {
    for (let x = 0; x < this.#map[0].length - 1; x++) {
      if (this.#testVerticalTowLines(x)) {
        this.#value = x + 1
        return true
      }
    }

    return false
  }

  #testVerticalTowLines(x: number): boolean {
    for (let y = 0; y < this.#map.length; y++) {
      if (this.#map[y][x] !== this.#map[y][x + 1]) {
        return false
      }
    }

    return this.#testVerticalRestOfLines(x, 1, true)
  }

  #testVerticalRestOfLines(x: number, i: number, lastValue: boolean): boolean {
    if ((x - i) < 0 || (x + 1 + i) >= this.#map[0].length) return lastValue

    for (let y = 0; y < this.#map.length; y++) {
      if (this.#map[y][x - i] !== this.#map[y][x + 1 + i]) {
        return false
      }
    }

    return this.#testVerticalRestOfLines(x, i + 1, true)
  }
}

class Maps {
  readonly #mirros: MirrorMap[] = []

  constructor(data: string) {
    let lines = ''
    for (const line of data.split('\n')) {
      if (line === '') {
        this.#mirros.push(new MirrorMap(lines))
        lines = ''
      } else {
        if (lines !== '') lines += '\n'
        lines += line
      }
    }
    this.#mirros.push(new MirrorMap(lines))
  }

  get value(): number {
    return this.#mirros.reduce((acc, mirror) => acc + mirror.value, 0)
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day13.txt', 'utf-8') as string
  return new Maps(data).value
}