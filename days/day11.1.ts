import { readFile } from 'node:fs/promises'

type UniverseSymbols = '.' | '#'

type Point = {
  x: number
  y: number
}

class Universe {
  #map: UniverseSymbols[][] = []
  #galaxies: Point[] = []

  constructor(data: string) {
    data.split('\n').forEach((line) => {
      let row: UniverseSymbols[] = []
      let rowTotallyEmpty = true
      line.split('').forEach((tile) => {
        row.push(tile as UniverseSymbols)
        if (tile === '#') rowTotallyEmpty = false
      })

      this.#map.push(row)
      if (rowTotallyEmpty) this.#map.push(new Array(row.length).fill('.'))
    })

    this.#expandUniverse()

    for (let y = 0; y < this.#map.length; y++) {
      for (let x = 0; x < this.#map[y].length; x++) {
        if (this.#map[y][x] === '#') this.#galaxies.push({ x, y })
      }
    }
  }

  galaxiesLength(): number {
    let lengths = 0
    for (let i = 0; i < this.#galaxies.length - 1; i++) {
      for (let j = i + 1; j < this.#galaxies.length; j++) {
        lengths += this.#galaxyLength(this.#galaxies[i], this.#galaxies[j])
      }
    }
    return lengths
  }

  #galaxyLength(g1: Point, g2: Point): number {
    if (g1.x === g2.x && g1.y === g2.y) return 0

    const dx = Math.abs(g2.x - g1.x)
    const dy = Math.abs(g2.y - g1.y)

    return dx + dy
  }

  #expandUniverse = () => {
    let emptyColumns: number[] = []
    XAxis: for (let x = 0; x < this.#map[0].length; x++) {
      for (let y = 0; y < this.#map.length; y++) {
        if (this.#map[y][x] === '#') continue XAxis
      }

      emptyColumns.push(x)
    }

    emptyColumns = emptyColumns.reverse()

    for (let y = 0; y < this.#map.length; y++) {
      for (const emptyColumn of emptyColumns) {
        this.#map[y].splice(emptyColumn, 0, '.')
      }
    }
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day11.txt', 'utf8') as string
  return (new Universe(data)).galaxiesLength()
}