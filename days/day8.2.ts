import { readFile } from 'node:fs/promises'

type Direction = 'R' | 'L'

class MapNode {
  readonly #rightNode: string
  readonly #leftNode: string

  constructor(leftNode: string, rightNode: string) {
    this.#rightNode = rightNode
    this.#leftNode = leftNode
  }

  getNode(direction: Direction): string {
    return direction === 'R' ? this.#rightNode : this.#leftNode
  }
}

class MapInstruction {
  #directions: Direction[] = []
  readonly #nodes: Record<string, MapNode> = {}
  #currentNodes: string[] = []
  #endNumber = 0

  constructor(data: string) {
    data.split('\n').forEach((line, i) => {
      if (i === 1) return

      if (i === 0) {
        this.#directions = line.split('') as Direction[]
        return
      }

      const [nodeName, nodesNames] = line.split(' = ')
      const [leftNode, rightNode] = nodesNames.split('(')[1].split(', ')

      this.#nodes[nodeName] = new MapNode(leftNode, rightNode.split(')')[0])
      if (nodeName.endsWith('A')) this.#currentNodes.push(nodeName)
    })

    this.#endNumber = this.#currentNodes.length
  }

  readInstruction(props: string): number {
    let indexes: number[] = []
    for (let currentNode of this.#currentNodes) {
      let i = 0
      while (!currentNode.endsWith('Z') && i < 1000000) {
        currentNode = this.#nodes[currentNode]?.getNode(this.#directions[i % this.#directions.length]) ?? ''

        if (currentNode === '') {
          console.error('Node not found')
          return 0
        }

        i++
      }

      if (currentNode.endsWith('Z')) {
        indexes.push(i)
      }
    }

    return indexes.reduce((acc, cur) => this.#lcm(acc, cur))
  }

  #gcd(a: number, b: number): number {
    if (!b) return a

    return this.#gcd(b, a % b)
  }

  #lcm(a: number, b: number): number {
    return (a * b) / this.#gcd(a, b)
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day8.txt', 'utf8') as string

  return (new MapInstruction(data)).readInstruction(data)
}