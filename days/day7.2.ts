import { readFile } from 'node:fs/promises'

type CardType = 'A' | 'K' | 'Q' | 'J' | 'T' | '9' | '8' | '7' | '6' | '5' | '4' | '3' | '2'

enum HandEnum {
  'HIGH_CARD',
  'ONE_PAIR',
  'TWO_PAIR',
  'THREE_OF_A_KIND',
  'FULL_HOUSE',
  'FOUR_OF_A_KIND',
  'FIVE_OF_A_KIND',
}

interface HandScore {
  hand: Hand;
  score: number;
}

class Hand {
  readonly handType: HandEnum
  hand: CardType[] = []
  score: number

  constructor(data) {
    const [hand, score] = data.split(' ')
    this.hand = hand.split('')
    this.score = Number(score)

    const cardList = {} as Record<CardType, number>
    this.hand.forEach(card => cardList[card] ? cardList[card]++ : cardList[card] = 1)

    const greatestCard = (cardHand: Record<CardType, number>): CardType => {
      let max = Number.NEGATIVE_INFINITY
      let maxKey: CardType = 'A'

      for (const [key, value] of Object.entries(cardHand)) {
        if (key === 'J') continue

        if (value > max) {
          max = value
          maxKey = key as CardType
        }
      }

      return maxKey
    }

    if (Object.keys(cardList).length === 1) {
      this.handType = HandEnum.FIVE_OF_A_KIND
      return
    }

    if (cardList['J']) {
      const great = greatestCard(cardList)
      cardList[great] += cardList['J']
      delete cardList['J']
    }

    if (Object.keys(cardList).length === 1) {
      this.handType = HandEnum.FIVE_OF_A_KIND
      return
    } else if (Object.keys(cardList).length === 2) {
      this.handType = Object.values(cardList).includes(4) ? HandEnum.FOUR_OF_A_KIND : HandEnum.FULL_HOUSE
    } else if (Object.keys(cardList).length === 3) {
      this.handType = Object.values(cardList).includes(3) ? HandEnum.THREE_OF_A_KIND : HandEnum.TWO_PAIR
    } else if (Object.keys(cardList).length === 4) {
      this.handType = HandEnum.ONE_PAIR
    } else {
      this.handType = HandEnum.HIGH_CARD
    }
  }

  win(hand: Hand): boolean {
    if (this.handType === hand.handType) return this.#checkCardByCard(hand)

    return this.handType > hand.handType
  }

  #checkCardByCard(hand: Hand): boolean {
    for (let i = 0; i < this.hand.length; i++) {
      if (this.hand[i] === hand.hand[i]) continue

      return this.#getCardValue(this.hand[i]) > this.#getCardValue(hand.hand[i])
    }
  }

  #getCardValue(card: CardType): number {
    switch (card) {
      case 'A':
        return 14
      case 'K':
        return 13
      case 'Q':
        return 12
      case 'J':
        return 1
      case 'T':
        return 10
      default:
        return Number(card)
    }
  }
}

class Card {
  readonly #hands: HandScore[] = []

  constructor(hands: string) {
    for (const handData of hands.split('\n')) {
      this.#hands.push({
        hand: new Hand(handData),
        score: 0,
      })
    }
  }

  checkHands(): number {
    for (let i = 0; i < this.#hands.length; i++) {
      for (let j = i + 1; j < this.#hands.length; j++) {
        this.#hands[i].hand.win(this.#hands[j].hand)
            ? this.#hands[i].score++
            : this.#hands[j].score++
      }
    }

    return this.#hands.sort((a, b) => a.score - b.score)
               .reduce((acc, hand, i) => ((i + 1) * hand.hand.score) + acc, 0)
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day7.txt', 'utf8') as string
  return (new Card(data)).checkHands()
}