import type { Point } from '../types'
import { readFile } from 'node:fs/promises'

type Item = 'O' | '.' | '#'
type RockMapMap = Record<string, Record<string, Omit<Item, 'O'> | Rock>>

let rockMap = new RockMap({})

class RockMap {
  readonly #map: RockMapMap

  constructor(data: RockMapMap) {
    this.#map = data
  }

  get maxY(): number {
    return Object.keys(this.#map).length
  }

  getItem(coordinate: Point): Rock | '#' | undefined {
    const item = this.#map[`${coordinate.y}`][`${coordinate.x}`]
    return item === '.' ? undefined : item as Rock | '#'
  }

  setItem(oldCoordinates: Point, newCoordinates: Point, rock: Rock): void {
    this.#map[`${oldCoordinates.y}`][`${oldCoordinates.x}`] = '.'
    this.#map[`${newCoordinates.y}`][`${newCoordinates.x}`] = rock
  }
}

class Rock {
  #coordinates: Point
  #linkTo: Rock | null = null
  #blocked: boolean = false

  constructor(coordinates: Point) {
    this.#coordinates = coordinates
  }

  get blocked() {
    return this.#blocked
  }

  get weight(): number {
    return rockMap.maxY - this.#coordinates.y
  }

  toTop(): boolean {
    if (this.#blocked) return false
    if (this.#coordinates.y === 0) return this.#setBlocked()

    const testWithTop = (): boolean => {
      if (this.#linkTo!.blocked) return this.#setBlocked()

      if (this.#linkTo!.toTop()) {
        return this.#moveTo({ y: this.#coordinates.y - 1 })
      }

      return this.#setBlocked()
    }
    if (this.#linkTo) return testWithTop()

    const item = rockMap.getItem({ ...this.#coordinates, y: this.#coordinates.y - 1 })
    if (item) {
      if (typeof item === 'string') return this.#setBlocked()

      this.#linkTo = item as Rock
      return testWithTop()
    }

    return this.#moveTo({ y: this.#coordinates.y - 1 })
  }

  #setBlocked(): false {
    this.#blocked = true
    return false
  }

  #moveTo(to: Partial<Point>): boolean {
    const oldCoordinates = { ...this.#coordinates }
    this.#coordinates.x = to.x ?? this.#coordinates.x
    this.#coordinates.y = to.y ?? this.#coordinates.y

    rockMap.setItem(oldCoordinates, this.#coordinates, this)

    return this.toTop()
  }
}

class Rocks {
  readonly #rocks: Rock[] = []
  readonly #maxY: number
  readonly #maxX: number

  constructor(data: string) {
    const map: RockMapMap = {}
    const rows = data.split('\n')
    this.#maxY = rows.length - 1
    this.#maxX = rows[0].length - 1
    for (let y = 0; y < rows.length; y++) {
      map[`${y}`] = {}
      const chars = rows[y].split('')
      for (let x = 0; x < chars.length; x++) {
        const char = chars[x]
        switch (char) {
          case 'O':
            const rock = new Rock({ x, y })
            map[`${y}`][`${x}`] = rock
            this.#rocks.push(rock)
            break
          case '.':
            map[`${y}`][`${x}`] = '.'
            break
          case '#':
            map[`${y}`][`${x}`] = '#'
            break
        }
      }
    }

    rockMap = new RockMap(map)

    this.#moveRocks()
  }

  getWeight(): number {
    return this.#rocks.reduce((acc, rock) => acc + rock.weight, 0)
  }

  #moveRocks() {
    this.#rocks.forEach((rock) => rock.toTop())
  }

}

export const dayFunction = async () => {
  const data = await readFile('data/day14.txt', 'utf8') as string
  return (new Rocks(data)).getWeight()
}