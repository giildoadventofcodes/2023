import { readFile } from 'node:fs/promises'

class MapEngine {
  readonly #map: string[][] = []
  readonly #symbols: string[] = ['$', '/', '*', '+', '=', '&', '%', '#', '@', '-']
  #coordinatesAlreadyRead: string[] = []
  #numbers: number[] = []

  constructor(content: string) {
    for (const line of content.split('\n')) {
      this.#map.push(line.split(''))
    }
  }

  readMap(): number {
    for (let y = 0; y < this.#map.length; y++) {
      for (let x = 0; x < this.#map[y].length; x++) {
        if (this.#symbols.includes(this.#map[y][x])) {
          this.#readAdjacent(x, y)
        }
      }
    }

    return this.#numbers.reduce((a, b) => a + b, 0)
  }

  #readAdjacent = (x: number, y: number): void => {
    const minX = (x - 1) < 0 ? 0 : x - 1
    const maxX = (x + 1) > this.#map.length ? this.#map.length : x + 1
    const minY = (y - 1) < 0 ? 0 : y - 1
    const maxY = (y + 1) > this.#map[x].length ? this.#map[x].length : y + 1

    const numbers = []

    for (let subY = minY; subY <= maxY; subY++) {
      for (let subX = minX; subX <= maxX; subX++) {
        if (/\d/.test(this.#map[subY][subX]) && !this.#coordinatesAlreadyRead.includes(`${subX},${subY}`)) {
          numbers.push(this.#readNumber(subX, subY))
        }
      }
    }

    if (this.#map[y][x] === '*' && numbers.length === 2) {
      this.#numbers.push(numbers[0] * numbers[1])
    }
  }

  #readNumber(x: number, y: number): number {
    this.#coordinatesAlreadyRead.push(`${x},${y}`)
    let number = this.#map[y][x]

    for (let subX = x - 1; subX >= 0; subX--) {
      if (/\d/.test(this.#map[y][subX]) && !this.#coordinatesAlreadyRead.includes(`${subX},${y}`)) {
        number = `${this.#map[y][subX]}${number}`
        this.#coordinatesAlreadyRead.push(`${subX},${y}`)
        continue
      }

      break
    }

    for (let subX = x + 1; subX < this.#map[y].length; subX++) {
      if (/\d/.test(this.#map[y][subX]) && !this.#coordinatesAlreadyRead.includes(`${subX},${y}`)) {
        number = `${number}${this.#map[y][subX]}`
        this.#coordinatesAlreadyRead.push(`${subX},${y}`)
        continue
      }

      break
    }

    return parseInt(number)
  }
}

export const dayFunction = async () => {
  const mapEngine = new MapEngine(await readFile('data/day3.txt', 'utf-8') as string)

  return mapEngine.readMap()
}