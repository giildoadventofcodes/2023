import { readFile } from 'node:fs/promises'

type UniverseSymbols = '.' | '#'

type Point = {
  x: number
  y: number
}

class Universe {
  #map: UniverseSymbols[][] = []
  #galaxies: Point[] = []
  #galaxyEmptyRows: number[] = []
  #galaxyEmptyColumns: number[] = []

  constructor(data: string) {
    data.split('\n').forEach((line, y) => {
      let row: UniverseSymbols[] = []
      let rowTotallyEmpty = true
      line.split('').forEach((tile, x) => {
        row.push(tile as UniverseSymbols)
        if (tile === '#') {
          rowTotallyEmpty = false
          this.#galaxies.push({ x, y })
        }
      })

      this.#map.push(row)
      if (rowTotallyEmpty) this.#galaxyEmptyRows.push(y)
    })

    this.#expandUniverse()
  }

  galaxiesLength(): number {
    let lengths = 0
    for (let i = 0; i < this.#galaxies.length - 1; i++) {
      for (let j = i + 1; j < this.#galaxies.length; j++) {
        lengths += this.#galaxyLength(this.#galaxies[i], this.#galaxies[j])
      }
    }
    return lengths
  }

  #galaxyLength(g1: Point, g2: Point): number {
    if (g1.x === g2.x && g1.y === g2.y) return 0

    const dx = Math.abs(g2.x - g1.x)
    const dy = Math.abs(g2.y - g1.y)

    let expansionCompensation = 0
    expansionCompensation += this.#galaxyEmptyRows.filter((row) => (row > g1.y && row < g2.y) || (row > g2.y && row < g1.y)).length
    expansionCompensation += this.#galaxyEmptyColumns.filter((column) => (column > g1.x && column < g2.x) || (column > g2.x && column < g1.x)).length

    return dx + dy + (expansionCompensation * 999999)
  }

  #expandUniverse = () => {
    XAxis: for (let x = 0; x < this.#map[0].length; x++) {
      for (let y = 0; y < this.#map.length; y++) {
        if (this.#map[y][x] === '#') continue XAxis
      }

      this.#galaxyEmptyColumns.push(x)
    }
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day11.txt', 'utf8') as string
  return (new Universe(data)).galaxiesLength()
}