import { readFile } from 'node:fs/promises'

interface CardInterface {
  goods: number[];
  tests: number[];
}

class Card {
  #cards: Array<CardInterface> = []
  #numberOfCards: Record<string, number> = {}

  constructor(instructions: string) {
    this.#cards = instructions.split('\n')
                              .map((line, i) => {
                                const [_, numbers] = line.split(':')
                                const [goods, tests] = numbers.split('|')
                                this.#numberOfCards[i + 1] = 1
                                return {
                                  goods: goods.split(' ').filter(nb => nb !== '').map((number) => parseInt(number)),
                                  tests: tests.split(' ').filter(nb => nb !== '').map((number) => parseInt(number)),
                                }
                              })
  }

  checkPoints(): number {
    this.#cards.forEach((card, i) => {
      const points = card.tests.reduce((acc, nb) => card.goods.includes(nb) ? ++acc : acc, 0)
      for (let j = i; j < i + points; j++) {
        this.#numberOfCards[j + 2] += this.#numberOfCards[i + 1]
      }
    })

    return Object.values(this.#numberOfCards).reduce((acc, nb) => acc + nb, 0)
  }
}

export const dayFunction = async () => {
  const instructions = await readFile('data/day4.txt', 'utf-8')

  return (new Card(instructions as string)).checkPoints()
}