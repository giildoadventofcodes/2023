import { readFile } from 'node:fs/promises'
import path from 'node:path'

export const dayFunction = async () => {
  const content = await readFile(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })

  const numberList = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
  const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
  return content.split('\n')
         .reduce((sum, line) => {
           let number: string = ''
           let splettation = ''
           const chars = line.split('')
           iLoop: for (let i = 0; i < chars.length; i++) {
             for (let j = 0; j < numberList.length; j++) {
               if (splettation.includes(numberList[j])) {
                 number += numbers[j]
                 splettation = ''
                 break iLoop
               }
             }

             if(numberList.some(nb => splettation.includes(nb))) {
                number += numbers[splettation]
                splettation = ''
               break
             }

             if (!isNaN(chars[i] as number)) {
                number += chars[i]
               break
             } else {
                splettation += chars[i]
             }
           }

          iLoop: for (let i = chars.length - 1; i >= 0; i--) {
             for (let j = 0; j < numberList.length; j++) {
               if (splettation.includes(numberList[j])) {
                 number += numbers[j]
                 splettation = ''
                 break iLoop
               }
             }

             if (!isNaN(chars[i] as number)) {
               number += chars[i]
               break
             } else {
                splettation = chars[i] + splettation
             }
           }

            return sum + parseInt(number, 10)
         }, 0)
}