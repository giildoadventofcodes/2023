import { readFile } from 'node:fs/promises'

type MapTile = '.' | '|' | '-' | 'L' | 'J' | '7' | 'F' | 'S'

enum MapDirection {
  UP,
  RIGHT,
  DOWN,
  LEFT,
}

interface Point {
  x: number
  y: number
}

class ReadMap {
  #map: MapTile[][] = []
  #startCoordinate: [number, number]
  #coordinateAlreadyVisited: Set<string> = new Set()
  #vertexCoordinates: Point[] = []
  #nbPointsInsidePolygon = 0

  constructor(data: string) {
    data.split('\n').forEach((line, y) => {
      const row: MapTile[] = []
      line.split('').map((tile: MapTile, x) => {
        row.push(tile)

        if (tile === 'S') {
          this.#startCoordinate = [x, y]
          this.#coordinateAlreadyVisited.add(`${x},${y}`)
        }
      })
      this.#map.push(row)
    })
  }

  public readMap(): number {
    const [x, y] = this.#startCoordinate
    this.#vertexCoordinates.push({ x, y })

    const possibleCoordinates: [[number, number], [number, number]] = [[x, y - 1], [x + 1, y], [x, y + 1], [x - 1, y]].filter(([x, y], i) => {
      if (this.#map[y]?.[x] === undefined) return false
      if (this.#map[y][x] === '.') return false

      switch (i) {
        case MapDirection.UP:
          return ['7', '|', 'F'].includes(this.#map[y][x])
        case MapDirection.RIGHT:
          return ['J', '-', '7'].includes(this.#map[y][x])
        case MapDirection.DOWN:
          return ['L', '|', 'J'].includes(this.#map[y][x])
        case MapDirection.LEFT:
          return ['F', '-', 'L'].includes(this.#map[y][x])
      }
    })

    this.#nextCoordinates({ x: possibleCoordinates[0][0], y: possibleCoordinates[0][1] }, { x, y })

    for (let y = 0, x = 0; y < this.#map.length && x < this.#map[y].length; y++, x++) {
      for (let x = 0; x < this.#map[y].length; x++) {
        if (this.#isPointIsInPolygon({ x, y })) {
          this.#nbPointsInsidePolygon++
        }
      }
    }

    return this.#nbPointsInsidePolygon
  }

  #nextCoordinates(currentCoordinates: Point, oldCoordinates: Point): Point {
    const { x, y } = currentCoordinates
    if (x === this.#startCoordinate[0] && y === this.#startCoordinate[1]) return currentCoordinates

    this.#coordinateAlreadyVisited.add(`${x},${y}`)

    const { x: oldX, y: oldY } = oldCoordinates
    const mapTile = this.#map[y][x]

    switch (mapTile) {
      case '-':
        return this.#nextCoordinates({ x: oldX < x ? x + 1 : x - 1, y }, currentCoordinates)
      case '|':
        return this.#nextCoordinates({ x, y: oldY < y ? y + 1 : y - 1 }, currentCoordinates)
      case 'J':
        this.#vertexCoordinates.push({ x, y })
        return x === oldX
            ? this.#nextCoordinates({ x: x - 1, y }, currentCoordinates)
            : this.#nextCoordinates({ x, y: y - 1 }, currentCoordinates)
      case 'L':
        this.#vertexCoordinates.push({ x, y })
        return x === oldX
            ? this.#nextCoordinates({ x: x + 1, y }, currentCoordinates)
            : this.#nextCoordinates({ x, y: y - 1 }, currentCoordinates)
      case '7':
        this.#vertexCoordinates.push({ x, y })
        return x === oldX
            ? this.#nextCoordinates({ x: x - 1, y }, currentCoordinates)
            : this.#nextCoordinates({ x, y: y + 1 }, currentCoordinates)
      case 'F':
        this.#vertexCoordinates.push({ x, y })
        return x === oldX
            ? this.#nextCoordinates({ x: x + 1, y }, currentCoordinates)
            : this.#nextCoordinates({ x, y: y + 1 }, currentCoordinates)
    }
  }

  /**
   * (yi > y) !== (yj > y)
   * ce premier test permet de vérifier si l'ordonnée du point est comprise entre l'ordonnée du point i et celle du point j
   * et donc s'il y a intersection.
   * Si ce n'est pas le cas, le point testé est en dessous ou au-dessus de la droite i-j et on ne fait pas le second test.
   *
   * S'il y a intersection, on fait le second test :
   * (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
   * Celui-ci permet de vérifier si x est inférieur à l'abscisse de l'intersection entre la droite i-j et la droite verticale passant par les points
   * Si c'est le cas x est à gauche de la droite i-j
   * Si ce n'est pas le cas x est à droite de la droite i-j
   * Si x est à gauche de la droite i-j, on inverse la valeur de "inside"
   *
   * Si on inverse "inside" un nombre impair de fois, le point est à l'intérieur du polygone
   * Sinon il est à l'extérieur
   *
   * L'équation de la droite "i-j" est : y = (yj - yi) / (xj - xi) * (x - xi) + yi,
   * elle est basée sur l'équation d'une droite : y = ax + b, avec :
   * - a = (yj - yi) / (xj - xi) → c'est la pente de la droite
   * - b = yi → c'est l'ordonnée à l'origine de la droite
   */
  #isPointIsInPolygon(p: Point): boolean {
    const { x, y } = p
    if (this.#coordinateAlreadyVisited.has(`${x},${y}`)) return false

    let inside = false

    for (let i = 0, j = this.#vertexCoordinates.length - 1; i < this.#vertexCoordinates.length; j = i++) {
      const xi = this.#vertexCoordinates[i].x, yi = this.#vertexCoordinates[i].y
      const xj = this.#vertexCoordinates[j].x, yj = this.#vertexCoordinates[j].y

      if (((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)) inside = !inside
    }

    return inside
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day10.txt', 'utf8') as string
  return (new ReadMap(data)).readMap()
}