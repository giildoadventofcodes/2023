import type { Point } from '../types'
import { readFile } from 'node:fs/promises'

type Item = 'O' | '.' | '#'
type RockMapMap = Record<string, Record<string, Omit<Item, 'O'> | Rock>>
type RockManipulation = {
  startCoordinates: Partial<Point>;
  rocks: Rock[];
}
type Directions = 'top' | 'bottom' | 'left' | 'right'

let rockMap = new RockMap({})

class RockMap {
  readonly #map: RockMapMap

  constructor(data: RockMapMap) {
    this.#map = data
  }

  get maxY(): number {
    return Object.keys(this.#map).length
  }

  get maxX(): number {
    return Object.keys(this.#map[0]).length
  }

  getItem(coordinate: Point): Rock | '#' | undefined {
    const item = this.#map[`${coordinate.y}`][`${coordinate.x}`]
    return item === '.' ? undefined : item as Rock | '#'
  }

  setItem(oldCoordinates: Point, newCoordinates: Point, rock: Rock): void {
    this.#map[`${oldCoordinates.y}`][`${oldCoordinates.x}`] = '.'
    this.#map[`${newCoordinates.y}`][`${newCoordinates.x}`] = rock
  }

  setPositions(direction: Directions): void {
    switch (direction) {
      case 'top':
        this.#setToTop(this.#readForTop())
        break
      case 'bottom':
        this.#setToBottom(this.#readForBottom())
        break
      case 'left':
        this.#setToLeft(this.#readForLeft())
        break
      case 'right':
        this.#setToRight(this.#readForRight())
        break
    }
  }

  #setToTop(manipulations: RockManipulation[][]): void {
    manipulations.forEach(columnManipulations => {
      columnManipulations.forEach(manipulation => {
        const { startCoordinates, rocks } = manipulation
        rocks.forEach((rock, index) => {
          const oldCoordinates = { ...rock.coordinates }
          rock.coordinates = { y: startCoordinates.y + index + 1 }
          rockMap.setItem(oldCoordinates, rock.coordinates, rock)
        })
      })
    })
  }

  #setToBottom(manipulations: RockManipulation[][]): void {
    manipulations.forEach(columnManipulations => {
      columnManipulations.forEach(manipulation => {
        const { startCoordinates, rocks } = manipulation
        rocks.forEach((rock, index) => {
          const oldCoordinates = { ...rock.coordinates }
          rock.coordinates = { y: startCoordinates.y - index - 1 }
          rockMap.setItem(oldCoordinates, rock.coordinates, rock)
        })
      })
    })
  }

  #setToLeft(manipulations: RockManipulation[][]): void {
    manipulations.forEach(columnManipulations => {
      columnManipulations.forEach(manipulation => {
        const { startCoordinates, rocks } = manipulation
        rocks.forEach((rock, index) => {
          const oldCoordinates = { ...rock.coordinates }
          rock.coordinates = { x: startCoordinates.x + index + 1 }
          rockMap.setItem(oldCoordinates, rock.coordinates, rock)
        })
      })
    })
  }

  #setToRight(manipulations: RockManipulation[][]): void {
    manipulations.forEach(columnManipulations => {
      columnManipulations.forEach(manipulation => {
        const { startCoordinates, rocks } = manipulation
        rocks.forEach((rock, index) => {
          const oldCoordinates = { ...rock.coordinates }
          rock.coordinates = { x: startCoordinates.x - index - 1 }
          rockMap.setItem(oldCoordinates, rock.coordinates, rock)
        })
      })
    })
  }

  #readForTop(): RockManipulation[][] {
    const manipulations: RockManipulation[][] = []
    for (let x = 0; x < this.maxX; x++) {
      let columnManipulations: RockManipulation[] = []
      let manipulation: RockManipulation = { startCoordinates: { y: -1 }, rocks: [] }
      for (let y = 0; y < this.maxY; y++) {
        const item = this.#map[`${y}`][`${x}`]
        if (item === '.') continue
        if (item === '#') {
          if (manipulation.rocks.length) columnManipulations.push(manipulation)
          manipulation = { startCoordinates: { y }, rocks: [] }
          continue
        }

        manipulation.rocks.push(item as Rock)
      }

      if (manipulation.rocks.length) columnManipulations.push(manipulation)
      manipulations.push(columnManipulations)
    }

    return manipulations
  }

  #readForBottom(): RockManipulation[][] {
    const manipulations: RockManipulation[][] = []
    for (let x = 0; x < this.maxX; x++) {
      let columnManipulations: RockManipulation[] = []
      let manipulation: RockManipulation = { startCoordinates: { y: this.maxY }, rocks: [] }
      for (let y = this.maxY - 1; y >= 0; y--) {
        const item = this.#map[`${y}`][`${x}`]
        if (item === '.') continue
        if (item === '#') {
          columnManipulations.push(manipulation)
          manipulation = { startCoordinates: { y }, rocks: [] }
          continue
        }

        manipulation.rocks.push(item as Rock)
      }
      columnManipulations.push(manipulation)
      manipulations.push(columnManipulations)
    }

    return manipulations
  }

  #readForLeft(): RockManipulation[][] {
    const manipulations: RockManipulation[][] = []
    for (let y = 0; y < this.maxY; y++) {
      let rowManipulations: RockManipulation[] = []
      let manipulation: RockManipulation = { startCoordinates: { x: -1 }, rocks: [] }
      for (let x = 0; x < this.maxX; x++) {
        const item = this.#map[`${y}`][`${x}`]
        if (item === '.') continue
        if (item === '#') {
          rowManipulations.push(manipulation)
          manipulation = { startCoordinates: { x }, rocks: [] }
          continue
        }

        manipulation.rocks.push(item as Rock)
      }
      rowManipulations.push(manipulation)
      manipulations.push(rowManipulations)
    }

    return manipulations
  }

  #readForRight(): RockManipulation[][] {
    const manipulations: RockManipulation[][] = []
    for (let y = 0; y < this.maxY; y++) {
      let rowManipulations: RockManipulation[] = []
      let manipulation: RockManipulation = { startCoordinates: { x: this.maxX }, rocks: [] }
      for (let x = this.maxX - 1; x >= 0; x--) {
        const item = this.#map[`${y}`][`${x}`]
        if (item === '.') continue
        if (item === '#') {
          rowManipulations.push(manipulation)
          manipulation = { startCoordinates: { x }, rocks: [] }
          continue
        }

        manipulation.rocks.push(item as Rock)
      }
      rowManipulations.push(manipulation)
      manipulations.push(rowManipulations)
    }

    return manipulations
  }
}

class Rock {
  readonly #coordinates: Point

  constructor(coordinates: Point) {
    this.#coordinates = coordinates
  }

  get weight(): number {
    return rockMap.maxY - this.#coordinates.y
  }

  get coordinates(): Point {
    return this.#coordinates
  }

  set coordinates(coordinates: Partial<Point>) {
    this.#coordinates.y = coordinates.y ?? this.#coordinates.y
    this.#coordinates.x = coordinates.x ?? this.#coordinates.x
  }
}

class Rocks {
  readonly #rocks: Rock[] = []
  readonly #maxY: number
  readonly #maxX: number

  constructor(data: string) {
    const map: RockMapMap = {}
    const rows = data.split('\n')
    this.#maxY = rows.length - 1
    this.#maxX = rows[0].length - 1
    for (let y = 0; y < rows.length; y++) {
      map[`${y}`] = {}
      const chars = rows[y].split('')
      for (let x = 0; x < chars.length; x++) {
        const char = chars[x]
        switch (char) {
          case 'O':
            const rock = new Rock({ x, y })
            map[`${y}`][`${x}`] = rock
            this.#rocks.push(rock)
            break
          case '.':
            map[`${y}`][`${x}`] = '.'
            break
          case '#':
            map[`${y}`][`${x}`] = '#'
            break
        }
      }
    }

    rockMap = new RockMap(map)

    for (let i = 0; i < 1000000000; i++) {
      this.#spinningRocks()
    }
  }

  getWeight(): number {
    return this.#rocks.reduce((acc, rock) => acc + rock.weight, 0)
  }

  #spinningRocks() {
    rockMap.setPositions('top')
    rockMap.setPositions('left')
    rockMap.setPositions('bottom')
    rockMap.setPositions('right')

    // first end = 87
    // second end = 70
    // third end = 69
    // end = 64
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day14.txt', 'utf8') as string
  return (new Rocks(data)).getWeight()
}