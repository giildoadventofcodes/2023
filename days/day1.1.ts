import { readFile } from 'node:fs/promises'
import path from 'node:path'

export const dayFunction = async () => {
  const content = await readFile(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
  return content.split('\n')
         .reduce((sum, line) => {
           let number: string = ''
           let splettation = ''
           const chars = line.split('')
           for (let i = 0; i < chars.length; i++) {

             if (!isNaN(chars[i] as number)) {
                number += chars[i]
               break
             } else {
                splettation += chars[i]
             }
           }

           for (let i = chars.length - 1; i >= 0; i--) {
             if (!isNaN(chars[i] as number)) {
               number += chars[i]
               break
             }
           }

            return sum + parseInt(number, 10)
         }, 0)
}