import { readFile } from 'node:fs/promises'

class Race {
  #raceTimes: number
  #raceRecord: number

  constructor(data: string) {
    data.split('\n').forEach((line, i) => {
      i === 0
          ? this.#raceTimes = parseInt(line.split(' ').join(''))
          : this.#raceRecord = parseInt(line.split(' ').join(''))
    })
  }

  testTime(): number {
    let win = 0
    for (let j = 0; j < this.#raceTimes; j++) {
      let distance = j * (this.#raceTimes - j)
      if (distance > this.#raceRecord) win++
    }

    return win
  }
}

export const dayFunction = async () => {
  const content = await readFile('data/day6.txt', 'utf8') as string
  return (new Race(content)).testTime()
}