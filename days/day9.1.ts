import { readFile } from 'node:fs/promises'

class Sequence {
  readonly #numbers: number[]
  #security = 0

  constructor(data) {
    this.#numbers = data.split(' ').map(Number)
  }

  analyze(payload?: number[]): number[] | number {
    // Sortie de sécurité
    if (this.#security > 100000) return payload!

    const diff: number[] = []
    for (let i = 0; i < (payload ?? this.#numbers).length - 1; i++) {
      const number = (payload ?? this.#numbers)[i]
      const nextNumber = (payload ?? this.#numbers)[i + 1]

      diff.push(nextNumber - number)
    }

    // Si tous les nombres sont à 0 c'est qu'on est à la fin de l'analyse
    if (diff.every(d => d === 0)) {
      diff.push(0)
      return diff
    }

    // On incrémente la sécurité et on analyse le nouveau tableau de différences
    this.#security++
    const diffChild = this.analyze(diff) as number[]
    diff.push(diff[diff.length - 1] + diffChild[diffChild.length - 1])

    // Si le payload est indéfini, c'est qu'on est à la première itération et qu'on doit que le nombre ajouté à la séquence de départ
    if (payload === undefined) {
      return diff[diff.length - 1] + this.#numbers[this.#numbers.length - 1]
    }

    // À la fin de l'anlayse on retourne ne nouveau tableau avec la nouvelle valeur à sa fin
    return diff
  }
}

class Sequences {
  #sequences: Sequence[] = []

  constructor(data) {
    data.split('\n').forEach(line => {
      this.#sequences.push(new Sequence(line))
    })
  }

  analyze(): number {
    const numbers: number[] = []
    for (let sequence of this.#sequences) {
      numbers.push(sequence.analyze() as number)
    }
    return numbers.reduce((a, b) => a + b, 0)
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day9.txt', 'utf8') as string
  return (new Sequences(data)).analyze()
}