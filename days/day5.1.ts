import { readFile } from 'node:fs/promises'

interface ConvertMap {
  destinationStart: number;
  destinationEnd: number;
  sourceStart: number;
  sourceEnd: number;
}

type ConvertionType =
    | 'seedToSoil'
    | 'soilToFertilizer'
    | 'fertilizerToWater'
    | 'waterToLight'
    | 'lightToTemperature'
    | 'temperatureToHumidity'
    | 'humidityToLocation'
    | 'end'

class Garden {
  #seeds: number[] = []
  #seedToSoilMap: ConvertMap[] = []
  #soilToFertilizerMap: ConvertMap[] = []
  #fertilizerToWaterMap: ConvertMap[] = []
  #waterToLightMap: ConvertMap[] = []
  #lightToTemperatureMap: ConvertMap[] = []
  #temperatureToHumidityMap: ConvertMap[] = []
  #humidityToLocationMap: ConvertMap[] = []
  #type: ConvertionType = 'seedToSoil'

  constructor(data: string) {
    let isSeedToSoilMap = false
    let isSoilToFertilizerMap = false
    let isFertilizerToWaterMap = false
    let isWaterToLightMap = false
    let isLightToTemperatureMap = false
    let isTemperatureToHumidityMap = false
    let isHumidityToLocationMap = false

    const toggleCondition = (conditionName: string): void => {
      isSeedToSoilMap = conditionName === 'isSeedToSoilMap'
      isSoilToFertilizerMap = conditionName === 'isSoilToFertilizerMap'
      isFertilizerToWaterMap = conditionName === 'isFertilizerToWaterMap'
      isWaterToLightMap = conditionName === 'isWaterToLightMap'
      isLightToTemperatureMap = conditionName === 'isLightToTemperatureMap'
      isTemperatureToHumidityMap = conditionName === 'isTemperatureToHumidityMap'
      isHumidityToLocationMap = conditionName === 'isHumidityToLocationMap'
    }

    const initMap = (map: ConvertMap[], payload: [string, string, string]): void => {
      const [destination, start, range] = payload.map(Number)
      map.push({
        sourceStart: start,
        sourceEnd: start + range - 1,
        destinationStart: destination,
        destinationEnd: destination + range - 1,
      })
    }

    data.split('\n').forEach((line, i) => {
      if (line === '') return
      if (i === 0) this.#seeds = line.split(': ')[1].split(' ').map(Number)

      switch (line) {
        case 'seed-to-soil map:':
          toggleCondition('isSeedToSoilMap')
          return
        case 'soil-to-fertilizer map:':
          toggleCondition('isSoilToFertilizerMap')
          return
        case 'fertilizer-to-water map:':
          toggleCondition('isFertilizerToWaterMap')
          return
        case 'water-to-light map:':
          toggleCondition('isWaterToLightMap')
          return
        case 'light-to-temperature map:':
          toggleCondition('isLightToTemperatureMap')
          return
        case 'temperature-to-humidity map:':
          toggleCondition('isTemperatureToHumidityMap')
          return
        case 'humidity-to-location map:':
          toggleCondition('isHumidityToLocationMap')
          return
      }

      if (isSeedToSoilMap) {
        initMap(this.#seedToSoilMap, line.split(' '))
        return
      }
      if (isSoilToFertilizerMap) {
        initMap(this.#soilToFertilizerMap, line.split(' '))
        return
      }
      if (isFertilizerToWaterMap) {
        initMap(this.#fertilizerToWaterMap, line.split(' '))
        return
      }
      if (isWaterToLightMap) {
        initMap(this.#waterToLightMap, line.split(' '))
        return
      }
      if (isLightToTemperatureMap) {
        initMap(this.#lightToTemperatureMap, line.split(' '))
        return
      }
      if (isTemperatureToHumidityMap) {
        initMap(this.#temperatureToHumidityMap, line.split(' '))
        return
      }
      if (isHumidityToLocationMap) {
        initMap(this.#humidityToLocationMap, line.split(' '))
        return
      }

    })
  }

  nearestSeed(): number {
    let nearestPosition = Infinity

    for (const seed of this.#seeds) {
      this.#type = 'seedToSoil'
      const seedPosition = this.#seedToLocation(seed)
      if (seedPosition < nearestPosition) nearestPosition = seedPosition
    }

    return nearestPosition
  }

  #seedToLocation(seed: number): number {
    if (this.#type === 'end') return seed

    for (const map of this.#getMap()) {
      if (seed >= map.sourceStart && seed <= map.sourceEnd) {
        this.#changeType()
        return this.#seedToLocation(map.destinationStart + (seed - map.sourceStart))
      }
    }

    this.#changeType()
    return this.#seedToLocation(seed)
  }

  #changeType() {
    switch (this.#type) {
      case 'seedToSoil':
        this.#type = 'soilToFertilizer'
        return
      case 'soilToFertilizer':
        this.#type = 'fertilizerToWater'
        return
      case 'fertilizerToWater':
        this.#type = 'waterToLight'
        return
      case 'waterToLight':
        this.#type = 'lightToTemperature'
        return
      case 'lightToTemperature':
        this.#type = 'temperatureToHumidity'
        return
      case 'temperatureToHumidity':
        this.#type = 'humidityToLocation'
        return
      case 'humidityToLocation':
        this.#type = 'end'
        return
    }
  }

  #getMap(): ConvertMap[] {
    switch (this.#type) {
      case 'seedToSoil':
        return this.#seedToSoilMap
      case 'soilToFertilizer':
        return this.#soilToFertilizerMap
      case 'fertilizerToWater':
        return this.#fertilizerToWaterMap
      case 'waterToLight':
        return this.#waterToLightMap
      case 'lightToTemperature':
        return this.#lightToTemperatureMap
      case 'temperatureToHumidity':
        return this.#temperatureToHumidityMap
      case 'humidityToLocation':
        return this.#humidityToLocationMap
    }
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day5.txt', 'utf8')
  const garden = new Garden(data as string)
  return garden.nearestSeed()
}