import { readFile } from 'node:fs/promises'

type MapTile = '.' | '|' | '-' | 'L' | 'J' | '7' | 'F' | 'S'

enum MapDirection {
  UP,
  RIGHT,
  DOWN,
  LEFT,
}

type PossibleCoordinates = [[number, number], [number, number]]

class ReadMap {
  #map: MapTile[][] = []
  #startCoordinate: [number, number]
  #coordinateAlreadyVisited: Set<string> = new Set()
  #max: number = 0

  constructor(data: string) {
    data.split('\n').forEach((line, y) => {
      const row: MapTile[] = []
      line.split('').map((tile: MapTile, x) => {
        row.push(tile)

        if (tile === 'S') {
          this.#startCoordinate = [x, y]
          this.#coordinateAlreadyVisited.add(`${x},${y}`)
        }
      })
      this.#map.push(row)
    })
  }

  public readMap(): number {
    const [x, y] = this.#startCoordinate

    const possibleCoordinates: PossibleCoordinates = [[x, y - 1], [x + 1, y], [x, y + 1], [x - 1, y]].filter(([x, y], i) => {
      if (this.#map[y]?.[x] === undefined) return false
      if (this.#map[y][x] === '.') return false

      switch (i) {
        case MapDirection.UP:
          return ['7', '|', 'F'].includes(this.#map[y][x])
        case MapDirection.RIGHT:
          return ['J', '-', '7'].includes(this.#map[y][x])
        case MapDirection.DOWN:
          return ['L', '|', 'J'].includes(this.#map[y][x])
        case MapDirection.LEFT:
          return ['F', '-', 'L'].includes(this.#map[y][x])
      }
    })

    for (const possibleCoordinate of possibleCoordinates) {
      this.#nextCoordinates(possibleCoordinates, [[x, y], [x, y]])
    }

    return ++this.#max
  }

  #nextCoordinates(currentCoordinates: PossibleCoordinates, oldCoordinates: PossibleCoordinates): PossibleCoordinates {
    const newCoordinates: PossibleCoordinates = []

    for (let i = 0; i < 2; i++) {
      const newCoordinate: [number, number] = [0, 0]
      const [x, y] = currentCoordinates[i]
      const [oldX, oldY] = oldCoordinates[i]
      const mapTile = this.#map[y][x]
      const key = `${x},${y}`
      if (this.#coordinateAlreadyVisited.has(key)) {
        return [[x, y], [x, y]]
      }

      this.#coordinateAlreadyVisited.add(key)
      switch (mapTile) {
        case '-':
          newCoordinate[1] = y
          newCoordinate[0] = oldX < x ? x + 1 : x - 1
          newCoordinates[i] = newCoordinate
          break
        case '|':
          newCoordinate[0] = x
          newCoordinate[1] = oldY < y ? y + 1 : y - 1
          newCoordinates[i] = newCoordinate
          break
        case 'J':
          if (x === oldX) {
            newCoordinate[0] = x - 1
            newCoordinate[1] = y
          } else {
            newCoordinate[0] = x
            newCoordinate[1] = y - 1
          }
          newCoordinates[i] = newCoordinate
          break
        case 'L':
          if (x === oldX) {
            newCoordinate[0] = x + 1
            newCoordinate[1] = y
          } else {
            newCoordinate[0] = x
            newCoordinate[1] = y - 1
          }
          newCoordinates[i] = newCoordinate
          break
        case '7':
          if (x === oldX) {
            newCoordinate[0] = x - 1
            newCoordinate[1] = y
          } else {
            newCoordinate[0] = x
            newCoordinate[1] = y + 1
          }
          newCoordinates[i] = newCoordinate
          break
        case 'F':
          if (x === oldX) {
            newCoordinate[0] = x + 1
            newCoordinate[1] = y
          } else {
            newCoordinate[0] = x
            newCoordinate[1] = y + 1
          }
          newCoordinates[i] = newCoordinate
      }
    }

    this.#max++
    return this.#nextCoordinates(newCoordinates, currentCoordinates)
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day10.txt', 'utf8') as string
  return (new ReadMap(data)).readMap()
}