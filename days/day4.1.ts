import { readFile } from 'node:fs/promises'

interface CardInterface {
  goods: number[];
  tests: number[];
}

class Card {
  #cards: Array<CardInterface> = []

  constructor(instructions: string) {
    this.#cards = instructions.split('\n')
                              .map((line) => {
                                const [_, numbers] = line.split(':')
                                const [goods, tests] = numbers.split('|')
                                return {
                                  goods: goods.split(' ').filter(nb => nb !== '').map((number) => parseInt(number)),
                                  tests: tests.split(' ').filter(nb => nb !== '').map((number) => parseInt(number)),
                                }
                              })
  }

  checkPoints(): number {
    return this.#cards.reduce<number>((acc, card, i) => acc + this.#checkCard(i), 0)
  }

  #checkCard(i: number): number {
    let points = 0
    for (const numberTested of this.#cards[i].tests) {
      if (this.#cards[i].goods.includes(numberTested)) {
        points = points === 0 ? 1 : points * 2
      }
    }
    return points
  }
}

export const dayFunction = async () => {
  const instructions = await readFile('data/day4.txt', 'utf-8')

  return (new Card(instructions as string)).checkPoints()
}