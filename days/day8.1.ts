import { readFile } from 'node:fs/promises'

type Direction = 'R' | 'L'

class MapNode {
  readonly #rightNode: string
  readonly #leftNode: string

  constructor(leftNode: string, rightNode: string) {
    this.#rightNode = rightNode
    this.#leftNode = leftNode
  }

  getNode(direction: Direction): string {
    return direction === 'R' ? this.#rightNode : this.#leftNode
  }
}

class MapInstruction {
  #directions: Direction[] = []
  readonly #nodes: Record<string, MapNode> = {}

  constructor(data: string) {
    data.split('\n').forEach((line, i) => {
      if (i === 1) return

      if (i === 0) {
        this.#directions = line.split('') as Direction[]
        return
      }

      const [nodeName, nodesNames] = line.split(' = ')
      const [leftNode, rightNode] = nodesNames.split('(')[1].split(', ')

      this.#nodes[nodeName] = new MapNode(leftNode, rightNode.split(')')[0])
    })
  }

  readInstruction(props: string): number {
    let i = 0
    let currentNode = 'AAA'

    while (currentNode !== 'ZZZ' && i < 1000000000) {
      currentNode = this.#nodes[currentNode]?.getNode(this.#directions[i % this.#directions.length]) ?? ''

      if (currentNode === '') {
        console.error('Node not found')
        return 0
      }

      i++
    }

    return i
  }
}

export const dayFunction = async () => {
  const data = await readFile('data/day8.txt', 'utf8') as string

  return (new MapInstruction(data)).readInstruction(data)
}