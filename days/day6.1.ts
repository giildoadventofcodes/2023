import { readFile } from 'node:fs/promises'

class Race {
  #raceTimes: number[] = []
  #raceRecord: number[] = []

  constructor(data: string) {
    data.split('\n').forEach((line, i) => {
      (i === 0 ? this.#raceTimes : this.#raceRecord).push(...line.split(' ').map(Number))
    })

  }

  testTime(): number {
    let wins: number[] = []
    let win = 0
    for (let i = 0; i < this.#raceTimes.length; i++) {
      win = 0
      for (let j = 0; j < this.#raceTimes[i]; j++) {
        let distance = j * (this.#raceTimes[i] - j)
        if (distance > this.#raceRecord[i]) win++
      }
      wins.push(win)
    }

    return wins.reduce((a, b) => a * b, 1)
  }
}

export const dayFunction = async () => {
  const content = await readFile('data/day6.txt', 'utf8') as string
  return (new Race(content)).testTime()
}