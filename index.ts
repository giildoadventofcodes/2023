import { dayFunction } from './days/day14.2'

let start = (new Date()).getTime()
let response = await dayFunction()
console.log(response)
let end = (new Date()).getTime()
console.log(`${end - start}ms`)
