import { writeFile } from 'node:fs/promises'

const dayNumber = prompt('Enter day number: ')
if (dayNumber) {
  const day = parseInt(dayNumber, 10)

  const contentFile = `export const dayFunction = async () => {
  return 0
  }`

  await writeFile(`days/day${day}.1.ts`, contentFile)
  await writeFile(`days/day${day}.2.ts`, contentFile)
  await writeFile(`data/day${day}.txt`, '')
  await writeFile(`data/day${day}_test.txt`, '')
}